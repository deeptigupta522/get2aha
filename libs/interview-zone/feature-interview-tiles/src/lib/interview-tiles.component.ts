import { Component, OnInit} from '@angular/core';
import { InterviewTilesFacade } from '@get2-aha/interview-zone/domain';

@Component({
  selector: 'interview-zone-interview-tiles',
  templateUrl: './interview-tiles.component.html',
  styleUrls: ['./interview-tiles.component.scss']
})
export class InterviewTilesComponent implements OnInit {
    
    
    interviewTilesList$ = this.interviewTilesFacade.interviewTilesList$;


    constructor(private interviewTilesFacade: InterviewTilesFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.interviewTilesFacade.load();
    }

}

