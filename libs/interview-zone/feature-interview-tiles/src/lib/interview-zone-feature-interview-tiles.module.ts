import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterviewZoneDomainModule } from '@get2-aha/interview-zone/domain';
import { InterviewTilesComponent } from './interview-tiles.component';

@NgModule({
  imports: [CommonModule, InterviewZoneDomainModule],
  declarations: [InterviewTilesComponent],
  exports: [InterviewTilesComponent],
})
export class InterviewZoneFeatureInterviewTilesModule {}
