import { Component, OnInit} from '@angular/core';
import { AddMeetingFacade } from '@get2-aha/interview-zone/domain';

@Component({
  selector: 'interview-zone-add-meeting',
  templateUrl: './add-meeting.component.html',
  styleUrls: ['./add-meeting.component.scss']
})
export class AddMeetingComponent implements OnInit {
    
    
    addMeetingList$ = this.addMeetingFacade.addMeetingList$;


    constructor(private addMeetingFacade: AddMeetingFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.addMeetingFacade.load();
    }

}

