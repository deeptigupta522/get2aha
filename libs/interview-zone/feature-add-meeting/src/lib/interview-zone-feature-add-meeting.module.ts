import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterviewZoneDomainModule } from '@get2-aha/interview-zone/domain';
import { AddMeetingComponent } from './add-meeting.component';

@NgModule({
  imports: [CommonModule, InterviewZoneDomainModule],
  declarations: [AddMeetingComponent],
  exports: [AddMeetingComponent],
})
export class InterviewZoneFeatureAddMeetingModule {}
