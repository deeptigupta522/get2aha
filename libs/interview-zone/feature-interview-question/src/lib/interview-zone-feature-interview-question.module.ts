import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterviewZoneDomainModule } from '@get2-aha/interview-zone/domain';
import { InterviewQuestionComponent } from './interview-question.component';

@NgModule({
  imports: [CommonModule, InterviewZoneDomainModule],
  declarations: [InterviewQuestionComponent],
  exports: [InterviewQuestionComponent],
})
export class InterviewZoneFeatureInterviewQuestionModule {}
