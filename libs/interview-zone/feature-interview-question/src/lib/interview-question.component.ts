import { Component, OnInit} from '@angular/core';
import { InterviewQuestionFacade } from '@get2-aha/interview-zone/domain';

@Component({
  selector: 'interview-zone-interview-question',
  templateUrl: './interview-question.component.html',
  styleUrls: ['./interview-question.component.scss']
})
export class InterviewQuestionComponent implements OnInit {
    
    
    interviewQuestionList$ = this.interviewQuestionFacade.interviewQuestionList$;


    constructor(private interviewQuestionFacade: InterviewQuestionFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.interviewQuestionFacade.load();
    }

}

