import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { InterviewQuestion } from '../entities/interview-question';
import { InterviewQuestionDataService } from '../infrastructure/interview-question.data.service';


@Injectable({ providedIn: 'root' })
export class InterviewQuestionFacade {

    private interviewQuestionListSubject = new BehaviorSubject<InterviewQuestion[]>([]); 
    interviewQuestionList$ = this.interviewQuestionListSubject.asObservable();

    constructor(private interviewQuestionDataService: InterviewQuestionDataService) {
    }

    load(): void {
        this.interviewQuestionDataService.load().subscribe(
            interviewQuestionList => {
                this.interviewQuestionListSubject.next(interviewQuestionList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
