import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { InterviewTiles } from '../entities/interview-tiles';
import { InterviewTilesDataService } from '../infrastructure/interview-tiles.data.service';


@Injectable({ providedIn: 'root' })
export class InterviewTilesFacade {

    private interviewTilesListSubject = new BehaviorSubject<InterviewTiles[]>([]); 
    interviewTilesList$ = this.interviewTilesListSubject.asObservable();

    constructor(private interviewTilesDataService: InterviewTilesDataService) {
    }

    load(): void {
        this.interviewTilesDataService.load().subscribe(
            interviewTilesList => {
                this.interviewTilesListSubject.next(interviewTilesList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
