import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { AddMeeting } from '../entities/add-meeting';
import { AddMeetingDataService } from '../infrastructure/add-meeting.data.service';


@Injectable({ providedIn: 'root' })
export class AddMeetingFacade {

    private addMeetingListSubject = new BehaviorSubject<AddMeeting[]>([]); 
    addMeetingList$ = this.addMeetingListSubject.asObservable();

    constructor(private addMeetingDataService: AddMeetingDataService) {
    }

    load(): void {
        this.addMeetingDataService.load().subscribe(
            addMeetingList => {
                this.addMeetingListSubject.next(addMeetingList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
