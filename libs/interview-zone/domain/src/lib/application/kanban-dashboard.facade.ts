import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { KanbanDashboard } from '../entities/kanban-dashboard';
import { KanbanDashboardDataService } from '../infrastructure/kanban-dashboard.data.service';


@Injectable({ providedIn: 'root' })
export class KanbanDashboardFacade {

    private kanbanDashboardListSubject = new BehaviorSubject<KanbanDashboard[]>([]); 
    kanbanDashboardList$ = this.kanbanDashboardListSubject.asObservable();

    constructor(private kanbanDashboardDataService: KanbanDashboardDataService) {
    }

    load(): void {
        this.kanbanDashboardDataService.load().subscribe(
            kanbanDashboardList => {
                this.kanbanDashboardListSubject.next(kanbanDashboardList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
