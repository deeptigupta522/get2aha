export interface KanbanDashboard {
    id: number;
    name: string;
    description: string;
}
