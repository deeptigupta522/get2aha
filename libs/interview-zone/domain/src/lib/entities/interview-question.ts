export interface InterviewQuestion {
    id: number;
    name: string;
    description: string;
}
