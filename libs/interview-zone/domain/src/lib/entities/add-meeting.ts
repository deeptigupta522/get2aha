export interface AddMeeting {
    id: number;
    name: string;
    description: string;
}
