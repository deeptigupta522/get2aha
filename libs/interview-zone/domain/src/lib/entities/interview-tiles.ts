export interface InterviewTiles {
    id: number;
    name: string;
    description: string;
}
