import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterviewZoneDomainModule } from '@get2-aha/interview-zone/domain';
import { KanbanDashboardComponent } from './kanban-dashboard.component';

@NgModule({
  imports: [CommonModule, InterviewZoneDomainModule],
  declarations: [KanbanDashboardComponent],
  exports: [KanbanDashboardComponent],
})
export class InterviewZoneFeatureKanbanDashboardModule {}
