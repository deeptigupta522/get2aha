import { Component, OnInit} from '@angular/core';
import { KanbanDashboardFacade } from '@get2-aha/interview-zone/domain';

@Component({
  selector: 'interview-zone-kanban-dashboard',
  templateUrl: './kanban-dashboard.component.html',
  styleUrls: ['./kanban-dashboard.component.scss']
})
export class KanbanDashboardComponent implements OnInit {
    
    
    kanbanDashboardList$ = this.kanbanDashboardFacade.kanbanDashboardList$;


    constructor(private kanbanDashboardFacade: KanbanDashboardFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.kanbanDashboardFacade.load();
    }

}

