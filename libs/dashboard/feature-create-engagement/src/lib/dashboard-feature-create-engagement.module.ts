import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardDomainModule } from '@get2-aha/dashboard/domain';
import { CreateEngagementComponent } from './create-engagement.component';

@NgModule({
  imports: [CommonModule, DashboardDomainModule],
  declarations: [CreateEngagementComponent],
  exports: [CreateEngagementComponent],
})
export class DashboardFeatureCreateEngagementModule {}
