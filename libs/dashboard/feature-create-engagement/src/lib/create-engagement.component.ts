import { Component, OnInit} from '@angular/core';
import { CreateEngagementFacade } from '@get2-aha/dashboard/domain';

@Component({
  selector: 'dashboard-create-engagement',
  templateUrl: './create-engagement.component.html',
  styleUrls: ['./create-engagement.component.scss']
})
export class CreateEngagementComponent implements OnInit {
    
    
    createEngagementList$ = this.createEngagementFacade.createEngagementList$;


    constructor(private createEngagementFacade: CreateEngagementFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.createEngagementFacade.load();
    }

}

