import { Component, OnInit} from '@angular/core';
import { ManageEngagementsFacade } from '@get2-aha/dashboard/domain';

@Component({
  selector: 'dashboard-manage-engagements',
  templateUrl: './manage-engagements.component.html',
  styleUrls: ['./manage-engagements.component.scss']
})
export class ManageEngagementsComponent implements OnInit {
    
    
    manageEngagementsList$ = this.manageEngagementsFacade.manageEngagementsList$;


    constructor(private manageEngagementsFacade: ManageEngagementsFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.manageEngagementsFacade.load();
    }

}

