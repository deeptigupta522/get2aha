import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardDomainModule } from '@get2-aha/dashboard/domain';
import { ManageEngagementsComponent } from './manage-engagements.component';

@NgModule({
  imports: [CommonModule, DashboardDomainModule],
  declarations: [ManageEngagementsComponent],
  exports: [ManageEngagementsComponent],
})
export class DashboardFeatureManageEngagementsModule {}
