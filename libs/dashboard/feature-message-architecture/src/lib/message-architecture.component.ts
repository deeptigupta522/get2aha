import { Component, OnInit} from '@angular/core';
import { MessageArchitectureFacade } from '@get2-aha/dashboard/domain';

@Component({
  selector: 'dashboard-message-architecture',
  templateUrl: './message-architecture.component.html',
  styleUrls: ['./message-architecture.component.scss']
})
export class MessageArchitectureComponent implements OnInit {
    
    
    messageArchitectureList$ = this.messageArchitectureFacade.messageArchitectureList$;


    constructor(private messageArchitectureFacade: MessageArchitectureFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.messageArchitectureFacade.load();
    }

}

