import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardDomainModule } from '@get2-aha/dashboard/domain';
import { MessageArchitectureComponent } from './message-architecture.component';

@NgModule({
  imports: [CommonModule, DashboardDomainModule],
  declarations: [MessageArchitectureComponent],
  exports: [MessageArchitectureComponent],
})
export class DashboardFeatureMessageArchitectureModule {}
