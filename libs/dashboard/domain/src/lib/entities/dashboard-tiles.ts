export interface DashboardTiles {
    id: number;
    name: string;
    description: string;
}
