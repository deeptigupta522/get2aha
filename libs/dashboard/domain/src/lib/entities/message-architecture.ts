export interface MessageArchitecture {
    id: number;
    name: string;
    description: string;
}
