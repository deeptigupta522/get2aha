export interface ManageEngagements {
    id: number;
    name: string;
    description: string;
}
