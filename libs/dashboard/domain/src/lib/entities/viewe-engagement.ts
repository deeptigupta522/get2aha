export interface VieweEngagement {
    id: number;
    name: string;
    description: string;
}
