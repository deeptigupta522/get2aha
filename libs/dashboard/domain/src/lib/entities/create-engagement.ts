export interface CreateEngagement {
    id: number;
    name: string;
    description: string;
}
