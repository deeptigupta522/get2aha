import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { VieweEngagement } from '../entities/viewe-engagement';
import { VieweEngagementDataService } from '../infrastructure/viewe-engagement.data.service';


@Injectable({ providedIn: 'root' })
export class VieweEngagementFacade {

    private vieweEngagementListSubject = new BehaviorSubject<VieweEngagement[]>([]); 
    vieweEngagementList$ = this.vieweEngagementListSubject.asObservable();

    constructor(private vieweEngagementDataService: VieweEngagementDataService) {
    }

    load(): void {
        this.vieweEngagementDataService.load().subscribe(
            vieweEngagementList => {
                this.vieweEngagementListSubject.next(vieweEngagementList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
