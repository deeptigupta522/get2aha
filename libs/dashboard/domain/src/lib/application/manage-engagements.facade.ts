import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ManageEngagements } from '../entities/manage-engagements';
import { ManageEngagementsDataService } from '../infrastructure/manage-engagements.data.service';


@Injectable({ providedIn: 'root' })
export class ManageEngagementsFacade {

    private manageEngagementsListSubject = new BehaviorSubject<ManageEngagements[]>([]); 
    manageEngagementsList$ = this.manageEngagementsListSubject.asObservable();

    constructor(private manageEngagementsDataService: ManageEngagementsDataService) {
    }

    load(): void {
        this.manageEngagementsDataService.load().subscribe(
            manageEngagementsList => {
                this.manageEngagementsListSubject.next(manageEngagementsList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
