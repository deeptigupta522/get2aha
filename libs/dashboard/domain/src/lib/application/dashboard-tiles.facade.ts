import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { DashboardTiles } from '../entities/dashboard-tiles';
import { DashboardTilesDataService } from '../infrastructure/dashboard-tiles.data.service';


@Injectable({ providedIn: 'root' })
export class DashboardTilesFacade {

    private dashboardTilesListSubject = new BehaviorSubject<DashboardTiles[]>([]); 
    dashboardTilesList$ = this.dashboardTilesListSubject.asObservable();

    constructor(private dashboardTilesDataService: DashboardTilesDataService) {
    }

    load(): void {
        this.dashboardTilesDataService.load().subscribe(
            dashboardTilesList => {
                this.dashboardTilesListSubject.next(dashboardTilesList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
