import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { MessageArchitecture } from '../entities/message-architecture';
import { MessageArchitectureDataService } from '../infrastructure/message-architecture.data.service';


@Injectable({ providedIn: 'root' })
export class MessageArchitectureFacade {

    private messageArchitectureListSubject = new BehaviorSubject<MessageArchitecture[]>([]); 
    messageArchitectureList$ = this.messageArchitectureListSubject.asObservable();

    constructor(private messageArchitectureDataService: MessageArchitectureDataService) {
    }

    load(): void {
        this.messageArchitectureDataService.load().subscribe(
            messageArchitectureList => {
                this.messageArchitectureListSubject.next(messageArchitectureList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
