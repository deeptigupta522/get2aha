import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { CreateEngagement } from '../entities/create-engagement';
import { CreateEngagementDataService } from '../infrastructure/create-engagement.data.service';


@Injectable({ providedIn: 'root' })
export class CreateEngagementFacade {

    private createEngagementListSubject = new BehaviorSubject<CreateEngagement[]>([]); 
    createEngagementList$ = this.createEngagementListSubject.asObservable();

    constructor(private createEngagementDataService: CreateEngagementDataService) {
    }

    load(): void {
        this.createEngagementDataService.load().subscribe(
            createEngagementList => {
                this.createEngagementListSubject.next(createEngagementList)
            },
            err => {
                console.error('err', err);
            }
        );
    }

}
