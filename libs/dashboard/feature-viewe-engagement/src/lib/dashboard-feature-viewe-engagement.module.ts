import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardDomainModule } from '@get2-aha/dashboard/domain';
import { VieweEngagementComponent } from './viewe-engagement.component';

@NgModule({
  imports: [CommonModule, DashboardDomainModule],
  declarations: [VieweEngagementComponent],
  exports: [VieweEngagementComponent],
})
export class DashboardFeatureVieweEngagementModule {}
