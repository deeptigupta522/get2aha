import { Component, OnInit} from '@angular/core';
import { VieweEngagementFacade } from '@get2-aha/dashboard/domain';

@Component({
  selector: 'dashboard-viewe-engagement',
  templateUrl: './viewe-engagement.component.html',
  styleUrls: ['./viewe-engagement.component.scss']
})
export class VieweEngagementComponent implements OnInit {
    
    
    vieweEngagementList$ = this.vieweEngagementFacade.vieweEngagementList$;


    constructor(private vieweEngagementFacade: VieweEngagementFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.vieweEngagementFacade.load();
    }

}

