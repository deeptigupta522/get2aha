import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardDomainModule } from '@get2-aha/dashboard/domain';
import { DashboardTilesComponent } from './dashboard-tiles.component';

@NgModule({
  imports: [CommonModule, DashboardDomainModule],
  declarations: [DashboardTilesComponent],
  exports: [DashboardTilesComponent],
})
export class DashboardFeatureDashboardTilesModule {}
