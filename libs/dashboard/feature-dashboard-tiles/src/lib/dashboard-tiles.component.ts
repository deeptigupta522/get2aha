import { Component, OnInit} from '@angular/core';
import { DashboardTilesFacade } from '@get2-aha/dashboard/domain';

@Component({
  selector: 'dashboard-dashboard-tiles',
  templateUrl: './dashboard-tiles.component.html',
  styleUrls: ['./dashboard-tiles.component.scss']
})
export class DashboardTilesComponent implements OnInit {
    
    
    dashboardTilesList$ = this.dashboardTilesFacade.dashboardTilesList$;


    constructor(private dashboardTilesFacade: DashboardTilesFacade) {
    }

    
    ngOnInit() {
        this.load();
    }

    load(): void {
        this.dashboardTilesFacade.load();
    }

}

