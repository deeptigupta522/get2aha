import { Component } from '@angular/core';

@Component({
  selector: 'get2-aha-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'get2-aha';
}
